# Synthesia Data Plugin

The **Synthesia Data** Plugin is for [Grav CMS](http://github.com/getgrav/grav). It's purpose is to parse `.synthesia` files from the YAML header and make data accesible in templates.

## Installation

Installing the Synthesia Data plugin can be done in one of two ways. The GPM (Grav Package Manager) installation method enables you to quickly and easily install the plugin with a simple terminal command, while the manual method enables you to do so via a zip file.

### GPM Installation (Preferred)

The simplest way to install this plugin is via the [Grav Package Manager (GPM)](http://learn.getgrav.org/advanced/grav-gpm) through your system's terminal (also called the command line).  From the root of your Grav install type:

```sh
bin/gpm install synthesia-data
```

This will install the Synthesia Data plugin into your `/user/plugins` directory within Grav. Its files can be found under `/your/site/grav/user/plugins/synthesia-data`.

### Manual Installation

To install this plugin, just download the zip version of this repository and unzip it under `/your/site/grav/user/plugins`. Then, rename the folder to `synthesia-data`. You can find these files on [GitHub](https://github.com/tcyrus/grav-plugin-synthesia-data) or via [GetGrav.org](http://getgrav.org/downloads/plugins#extras).

You should now have all the plugin files under

```
/your/site/grav/user/plugins/synthesia-data
```
	
> NOTE: This plugin is a modular component for Grav which requires [Grav](http://github.com/getgrav/grav) and the [Error](https://github.com/getgrav/grav-plugin-error) and [Problems](https://github.com/getgrav/grav-plugin-problems) to operate.

### Admin Plugin

If you use the admin plugin, you can install directly through the admin plugin by browsing the `Plugins` tab and clicking on the `Add` button.

## Configuration

Before configuring this plugin, you should copy the `user/plugins/synthesia-data/synthesia-data.yaml` to `user/config/plugins/synthesia-data.yaml` and only edit that copy.

Here is the default configuration and an explanation of available options:

```yaml
enabled: true
```

Note that if you use the admin plugin, a file with your configuration, and named `synthesia-data.yaml` will be saved in the `user/config/plugins/` folder once the configuration is saved in the admin.

## Usage

**Note:** This plugin was built for use with my custom theme and has not been tested with other themes. If you are having trouble using this, feel free to contact me with questions.

Add something like this to your page frontmatter:

```yaml
synthesia: 'myfile.synthesia'
```

## Credits

Used `grav-plugin-table-importer`, Grav Docs, and PHP Docs for reference

## TODO

- [ ] Add Customizability
