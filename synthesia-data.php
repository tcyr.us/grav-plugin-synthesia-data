<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use Grav\Common\Utils;
use RocketTheme\Toolbox\Event\Event;

/**
 * Class SynthesiaDataPlugin
 * @package Grav\Plugin
 */
class SynthesiaDataPlugin extends Plugin
{
    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0]
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            return;
        }

        // Enable the main event we are interested in
        $this->enable([
            'onPageInitialized' => ['onPageInitialized', 0]
        ]);
    }

    /**
     * Do some work for this event
     */
    public function onPageInitialized($event)
    {
        // @var Page\Page $page
        $page = $this->grav['page'];

        if (!isset($page->header()->synthesia)) {
            return;
        }

        $synthesiaFile = $page->header()->synthesia;
        $parsed = [];

        if (Utils::endswith($synthesiaFile, '.synthesia')) {
            $path = $this->getPath($synthesiaFile, $page);
            if (!is_null($path)) {
                $xml = simplexml_load_file($path);
                $songs = $xml->xpath('/SynthesiaMetadata/Songs/Song');
                foreach ($songs as $song) {
                    array_push($parsed, array(
                            'uniqueId' => $song['UniqueId'],
                            'title' => $song['Title'],
                            'subtitle' => $song['Subtitle'],
                            'arranger' => $song['Arranger'],
                            'composer' => $song['Composer'],
                            'copyright' => $song['Copyright'],
                            'madeFamousby' => $song['MadeFamousBy']
                    ));
                }
            }
        }

        $page->header()->synthesia_data = $parsed;
    }


    private function getPath($fn, $page)
    {
        $path = '';
        if (strpos($fn, '://') !== false) {
            $path = $this->grav['locator']->findResource($fn, true);
        } else {
            $path = $page->path() . DS . $fn;
        }

        return (file_exists($path) ? $path : null);
    }
}
